﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public interface ISwipeListener : IEventSystemHandler {
    void SwipeLeft();
    void SwipeRight();
    void SwipeUp();
    void SwipeDown();
    void SwipeDelta(Vector3 delta);
    void SwipeStarted(Vector3 start);
    void SwipeEnded(Vector3 start, Vector3 end);
    void Tap(Vector3 location);
}
