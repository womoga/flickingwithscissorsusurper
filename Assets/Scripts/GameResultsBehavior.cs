﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameResultsBehavior : MonoBehaviour
{
    public GameObject ContinueButton;
    public GameObject ContinueText;
    public GameObject NewHighScore;
    public GameObject HighScore;
    public GameObject HighScoreValue;
    public GameObject YourScore;
    public GameObject YourScoreValue;
    public GameObject ReplayButton;
    public GameObject ExitButton;
    public GameObject Loser;
    public GameObject Winner;
    public GameObject Sleeper;

    public Sprite RockSprite;
    public Sprite PaperSprite;
    public Sprite ScissorsSprite;

    private bool ContinueApproved = false;
    // Start is called before the first frame update
    void Start()
    {
        ContinueApproved = false;
        ContinueButton.SetActive(false);
        ContinueText.SetActive(false);
        NewHighScore.SetActive(false);
        HighScore.SetActive(false);
        HighScoreValue.SetActive(false);
        YourScore.SetActive(false);
        YourScoreValue.SetActive(false);
        Loser.SetActive(false);
        Winner.SetActive(false);
        Sleeper.SetActive(false);

        if (Storage.Continues > 0) {
            ContinueButton.SetActive(true);
            ContinueText.SetActive(true);
        }

        switch (Storage.Winner) {
          case GamePlayBehavior.Elements.Error: break;
          case GamePlayBehavior.Elements.Rock:
            Winner.GetComponent<SpriteRenderer>().sprite = RockSprite;
            Winner.SetActive(true);
            break;
          case GamePlayBehavior.Elements.Paper:
            Winner.GetComponent<SpriteRenderer>().sprite = PaperSprite;
            Winner.SetActive(true);
            break;
          case GamePlayBehavior.Elements.Scissors:
            Winner.GetComponent<SpriteRenderer>().sprite = ScissorsSprite;
            Winner.SetActive(true);
            break;
        }

        switch (Storage.Loser) {
          case GamePlayBehavior.Elements.Error: break;
          case GamePlayBehavior.Elements.Rock:
            Loser.GetComponent<SpriteRenderer>().sprite = RockSprite;
            Loser.SetActive(true);
            break;
          case GamePlayBehavior.Elements.Paper:
            Loser.GetComponent<SpriteRenderer>().sprite = PaperSprite;
            Loser.SetActive(true);
            break;
          case GamePlayBehavior.Elements.Scissors:
            Loser.GetComponent<SpriteRenderer>().sprite = ScissorsSprite;
            Loser.SetActive(true);
            break;
        }

        switch (Storage.Sleeper) {
          case GamePlayBehavior.Elements.Error: break;
          case GamePlayBehavior.Elements.Rock:
            Sleeper.GetComponent<SpriteRenderer>().sprite = RockSprite;
            Sleeper.SetActive(true);
            break;
          case GamePlayBehavior.Elements.Paper:
            Sleeper.GetComponent<SpriteRenderer>().sprite = PaperSprite;
            Sleeper.SetActive(true);
            break;
          case GamePlayBehavior.Elements.Scissors:
            Sleeper.GetComponent<SpriteRenderer>().sprite = ScissorsSprite;
            Sleeper.SetActive(true);
            break;
        }



        YourScore.SetActive(true);
        YourScoreValue.GetComponent<TextMeshProUGUI>().text = Storage.CurrentScore.ToString();
        YourScoreValue.SetActive(true);
        if (Storage.CurrentScore > Storage.HighScore)
        {
            Storage.HighScore = Storage.CurrentScore;
            NewHighScore.SetActive(true);
        } else
        {
            HighScoreValue.GetComponent<TextMeshProUGUI>().text = Storage.HighScore.ToString();
            HighScore.SetActive(true);
            HighScoreValue.SetActive(true);
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (ContinueApproved) {
          ContinueApproved = false;
          Storage.Continues = Storage.Continues-1;
          StartCoroutine(LoadYourAsyncScene("GamePlay"));
        }
    }

    public void OnPlayExit()
    {
        StartCoroutine(LoadYourAsyncScene("MainMenu"));
    }

    public void OnRestart()
    {
        Storage.CurrentScore = 0;
        Storage.Continues = 1;
        StartCoroutine(LoadYourAsyncScene("GamePlay"));
    }

    public void ContinueGame()
    {
        ContinueApproved = true;
    }
    IEnumerator LoadYourAsyncScene(string name)
    {
        // The Application loads the Scene in the background as the current Scene runs.
        // This is particularly good for creating loading screens.
        // You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
        // a sceneBuildIndex of 1 as shown in Build Settings.

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(name);

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
   

}
