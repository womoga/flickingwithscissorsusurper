﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SwipeDetector: MonoBehaviour {

    public enum SwipeDirection { None, Up, Down, Left, Right, Delta, Tap};

    public float MinimumSwipeDistance;
    private Vector2 position1;
    private Vector2 position2;
    private Vector3 lastPosition;
    private Vector2 current;
    private Vector2 delta;
    private bool swipeStarted = false;
    private bool swipeEnded = false;

    private SwipeDirection direction;
	// Update is called once per frame
	void Update () {
        DetectMouseSwipe();
        DetectFingerSwipe();
        CalculateSwipe();
        switch (direction)
        {
            case SwipeDirection.Up:
                ExecuteEvents.Execute<ISwipeListener>(gameObject, null, (x, y) => x.SwipeUp());
                break;
            case SwipeDirection.Down:
                ExecuteEvents.Execute<ISwipeListener>(gameObject, null, (x, y) => x.SwipeDown());
                break;
            case SwipeDirection.Left:
                ExecuteEvents.Execute<ISwipeListener>(gameObject, null, (x, y) => x.SwipeLeft());
                break;
            case SwipeDirection.Right:
                ExecuteEvents.Execute<ISwipeListener>(gameObject, null, (x, y) => x.SwipeRight());
                break;
            case SwipeDirection.Delta:
                ExecuteEvents.Execute<ISwipeListener>(gameObject, null, (x, y) => x.SwipeDelta(delta));
                break;
            case SwipeDirection.Tap:
                ExecuteEvents.Execute<ISwipeListener>(gameObject, null, (x, y) => x.Tap(position1));
                break;
            default: break;
        }
        direction = SwipeDirection.None;
    }

    void CalculateSwipe()
    {
        if (swipeStarted)
        {
            direction = SwipeDirection.Delta;
            delta = new Vector3(position2.x - lastPosition.x, position2.y - lastPosition.y);
            if (swipeEnded)
            {
                current = new Vector3(position2.x - position1.x, position2.y - position1.y);
                if (current.magnitude < MinimumSwipeDistance)
                {
                    direction = SwipeDirection.Tap;
                }
                else
                {
                    current.Normalize();
                    if (current.y > 0 && current.x > -0.5f && current.x < 0.5f)
                    {
                        direction = SwipeDirection.Up;
                    }
                    else if (current.y < 0 && current.x > -0.5f && current.x < 0.5f)
                    {
                        direction = SwipeDirection.Down;
                    }
                    else if (current.x < 0 && current.y > -0.5f && current.y < 0.5f)
                    {
                        direction = SwipeDirection.Left;
                    }
                    else if (current.x > 0 && current.y > -0.5f && current.y < 0.5f)
                    {
                        direction = SwipeDirection.Right;
                    }
                    else
                    {
                        direction = SwipeDirection.None;
                    }
                }
                swipeStarted = swipeEnded = false;
            }
            lastPosition = position2;
        }
    }

    void DetectFingerSwipe()
    {        
        if (Input.touches.Length == 1)
        {
            Touch t = Input.GetTouch(0);
            position2 = t.position;
            switch (t.phase)
            {
                case TouchPhase.Began:
                    swipeStarted = true;
                    position1 = t.position;
                    lastPosition = position1;
                    ExecuteEvents.Execute<ISwipeListener>(gameObject, null, (x, y) => x.SwipeStarted(position1));
                    break;
                case TouchPhase.Ended:
                    swipeEnded = true;
                    ExecuteEvents.Execute<ISwipeListener>(gameObject, null, (x, y) => x.SwipeEnded(position1, position2));
                    break;
                default:
                    break;
            }
        }
    }

    void DetectMouseSwipe()
    {
        position2 = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

        if (!swipeStarted && Input.GetMouseButtonDown(0))
        {
            swipeStarted = true;
            position1 = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            lastPosition = position1;
            ExecuteEvents.Execute<ISwipeListener>(gameObject, null, (x, y) => x.SwipeStarted(position1));
        } else if(Input.GetMouseButtonUp(0))
        {
            swipeEnded = true;
            ExecuteEvents.Execute<ISwipeListener>(gameObject, null, (x, y) => x.SwipeEnded(position1, position2));
        }
    }
}
