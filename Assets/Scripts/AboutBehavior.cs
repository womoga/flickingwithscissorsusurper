﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;

public class AboutBehavior : MonoBehaviour, IPointerClickHandler
{

    public TextMeshProUGUI textMeshPro;
    public TextMeshProUGUI textSound;
    public Canvas canvas;
    public new Camera camera;


    // Start is called before the first frame update
    void Start()
    {
        if (Storage.SoundOn)
        {
            textSound.text = "SOUND OFF";
        } else
        {
            textSound.text = "SOUND ON";
        }

        string txt = textMeshPro.text;
        textMeshPro.text = txt + "\n\nVersion: " + Application.version;

    }

        // Update is called once per frame
        void Update()
        {

        }

        public void LoadMainMenu()
        {
            StartCoroutine(LoadYourAsyncScene("MainMenu"));
        }

        IEnumerator LoadYourAsyncScene(string name)
        {
            // The Application loads the Scene in the background as the current Scene runs.
            // This is particularly good for creating loading screens.
            // You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
            // a sceneBuildIndex of 1 as shown in Build Settings.

            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(name);

            // Wait until the asynchronous scene fully loads
            while (!asyncLoad.isDone)
            {
                yield return null;
            }
        }

   
        public void ResetScores()
        {
            Storage.EasyHighScore = 0;
            Storage.HardHighScore = 0;
        }


    public void ToggleSound()
    {
        if (Storage.SoundOn)
        {
            Storage.SoundOn = false;
            textSound.text = "SOUND ON";
        } else
        {
            Storage.SoundOn = true;
            textSound.text = "SOUND OFF";
        }
    }

        void Awake()
        {
       /*     textMeshPro = gameObject.GetComponent<TextMeshProUGUI>();
            canvas = gameObject.GetComponentInParent<Canvas>();
            if (canvas.renderMode == RenderMode.ScreenSpaceOverlay)
            {
                camera = null;
            }
            else
            {
                camera = canvas.worldCamera;
            }*/
        }

    /*List<Color32[]> SetLinkToColor(int linkIndex, Color32 color)
    {
        TMP_LinkInfo linkInfo = textMeshPro.textInfo.linkInfo[linkIndex];

        var oldVertColors = new List<Color32[]>(); // store the old character colors

        for (int i = 0; i < linkInfo.linkTextLength; i++)
        { // for each character in the link string
            int characterIndex = linkInfo.linkTextfirstCharacterIndex + i; // the character index into the entire text
            var charInfo = textMeshPro.textInfo.characterInfo[characterIndex];
            int meshIndex = charInfo.materialReferenceIndex; // Get the index of the material / sub text object used by this character.
            int vertexIndex = charInfo.vertexIndex; // Get the index of the first vertex of this character.

            Color32[] vertexColors = textMeshPro.textInfo.meshInfo[meshIndex].colors32; // the colors for this character
            oldVertColors.Add(vertexColors.ToArray());

            if (charInfo.isVisible)
            {
                vertexColors[vertexIndex + 0] = color;
                vertexColors[vertexIndex + 1] = color;
                vertexColors[vertexIndex + 2] = color;
                vertexColors[vertexIndex + 3] = color;
            }
        }

        // Update Geometry
        textMeshPro.UpdateVertexData(TMP_VertexDataUpdateFlags.All);

        return oldVertColors;
    }*/

    public void OnPointerClick(PointerEventData eventData)
        {
            int linkIndex = TMP_TextUtilities.FindIntersectingLink(textMeshPro, Input.mousePosition, camera);
            if (linkIndex != -1)
            {
                TMP_LinkInfo linkInfo = textMeshPro.textInfo.linkInfo[linkIndex];
                switch (linkInfo.GetLinkID())
                {
                    case "pixabay":
                        Application.OpenURL("http://pixabay.com");
                        break;
                    case "kenney":
                        Application.OpenURL("http://www.kenney.nl");
                        break;
                    case "privacy":
                        Application.OpenURL("https://unity3d.com/legal/privacy-policy");
                        break;
                    default:
                        //Do something else
                        break;
                }
            }   
        }
    
}
