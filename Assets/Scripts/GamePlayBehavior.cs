﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class GamePlayBehavior : MonoBehaviour, ISwipeListener
{
    public float MovementSpeed;
    public Sprite RockSprite;
    public Sprite PaperSprite;
    public Sprite ScissorsSprite;


    public GameObject Pointer;
    public GameObject LeftThrown;
    public GameObject MiddleThrown;
    public GameObject RightThrown;
    public GameObject ChallengerThrown;

    public GameObject WinsText;
    
    public GameObject FallPoint;

    public GameObject LeftTimeIndicator;
    public GameObject RightTimeIndicator;

    public enum Elements { Rock, Paper, Scissors, Error };
    public enum Results { Win, Lose, Draw, Delay };

    private Elements LeftElement;
    private Elements MiddleElement;
    private Elements RightElement;
    private Elements ChallengerElement;

    private Results Result;
    private Elements AttackedElement;
    private GameObject AttackedGO;
    private Vector3 HomePosition;

    private double MaxTime;
    private System.DateTime StartTime;
    private int Wins = 0;
    private bool StopClock = true;
    private bool GameOver = false;
    private System.Random random = new System.Random();
    private bool Swiping = false;

    private bool Dying = false;

    private AudioSource source;

    public AudioClip clipWin;
    public AudioClip clipLose;
    public AudioClip clipDraw;

    public static int INITIAL_CONTINUES = 1;
    // Start is called before the first frame update
    void Start()
    {
        HomePosition = ChallengerThrown.GetComponent<Transform>().position;
        source = GetComponent<AudioSource>();
        InitGame(Storage.CurrentScore);
    }


    public void InitGame(int wins)
    {
        if (wins == 0)
        {
            LeftElement = Elements.Rock;
            MiddleElement = Elements.Paper;
            RightElement = Elements.Scissors;
            Pointer.GetComponent<Transform>().position = HomePosition;
            ChallengerElement = GetChallenger();
        } else {
            LeftElement = Storage.Left;
            MiddleElement = Storage.Middle;
            RightElement = Storage.Right;
            ChallengerElement = Storage.Challenger;
        } 
        ChallengerThrown.GetComponent<Transform>().position = HomePosition;
        Swiping = false;
        AttackedGO = null;
        Wins = wins;
        GameOver = false;
        Dying = false;
        LeftThrown.SetActive(true);
        MiddleThrown.SetActive(true);
        RightThrown.SetActive(true);
        ChallengerThrown.SetActive(true);
        SetSprites();
        WinsText.SetActive(true);
        StopClock = true;
    }

    public void InitGame()
    {
        InitGame(0);
    }

    double EasySpeedForWins(int wins)
    {
        if (wins == 0)
            return MovementSpeed*1.0;
        if (wins <= 2)
            return MovementSpeed * 1.1;
        if (wins == 3)
            return MovementSpeed * 1.15;
        if (wins == 4)
            return MovementSpeed * 1.2;
        if (wins == 5)
            return MovementSpeed * 1.25;
        if (wins == 6)
            return MovementSpeed * 1.3;
        if (wins == 7)
            return MovementSpeed * 1.35;
        if (wins < 10)
            return MovementSpeed * 1.4;
        if (wins < 20)
            return MovementSpeed * 1.45;
        if (wins < 90)
            return MovementSpeed * 1.5;
        if (wins < 140)
            return MovementSpeed * 1.55;
        return MovementSpeed * 1.6;
    }

    double EasyTimeForWins(int wins)
    {
        if (wins == 0)
            return 6000;
        if (wins <= 2)
            return 5500;
        if (wins == 3)
            return 5000;
        if (wins == 4)
            return 4500;
        if (wins == 5)
            return 4000;
        if (wins == 6)
            return 3500;
        if (wins == 7)
            return 3000;
        if (wins < 10)
            return 2500;
        if (wins < 20)
            return 2000;
        if (wins < 50)
            return 1750;
        if (wins < 90)
            return 1500;
        if (wins < 140)
            return 1250;
        return 1000;
    }


    double HardTimeForWins(int wins)
    {
        if (wins == 0)
            return 5000;
        if (wins <= 2)
            return 4500;
        if (wins == 3)
            return 4000;
        if (wins == 4)
            return 3500;
        if (wins == 5)
            return 3000;
        if (wins == 6)
            return 2500;
        if (wins == 7)
            return 2000;
        if (wins == 8)
            return 1500;
        if (wins < 10)
            return 1000;
        if (wins < 15)
            return 800;
        if (wins < 30)
            return 775;
        if (wins < 50)
            return 750;
        return 725;
    }

    double HardSpeedForWins(int wins)
    {
        if (wins == 0)
            return MovementSpeed * 1.0;
        if (wins <= 2)
            return MovementSpeed * 1.1;
        if (wins == 3)
            return MovementSpeed * 1.2;
        if (wins == 4)
            return MovementSpeed * 1.3;
        if (wins == 5)
            return MovementSpeed * 1.4;
        if (wins == 6)
            return MovementSpeed * 1.5;
        if (wins == 7)
            return MovementSpeed * 1.6;
        if (wins == 8)
            return MovementSpeed * 1.7;
        if (wins < 10)
            return MovementSpeed * 1.8;
        if (wins < 15)
            return MovementSpeed * 1.9;
        if (wins < 30)
            return MovementSpeed * 2.0;
        if (wins < 50)
            return MovementSpeed * 2.1;
        return MovementSpeed * 2.2;
    }

    double TimeForWins(int wins)
    {
        switch (Storage.Difficulty)
        {
            case Storage.DifficultyLevel.Easy: return EasyTimeForWins(wins);
            case Storage.DifficultyLevel.Hard: return HardTimeForWins(wins);
        }

        return HardTimeForWins(wins);
    }

    Elements LosesTo(Elements e)
    {
        switch(e)
        {
            case Elements.Rock: return Elements.Paper;
            case Elements.Paper: return Elements.Scissors;
            case Elements.Scissors: return Elements.Rock;
        }
        return Elements.Error;
    }

    Results Challenge(Elements challenger, Elements champ)
    {
        if (challenger == Elements.Rock && champ == Elements.Scissors)
            return Results.Win;
        if (challenger == Elements.Paper && champ == Elements.Rock)
            return Results.Win;
        if (challenger == Elements.Scissors && champ == Elements.Paper)
            return Results.Win;
        if (challenger == champ)
            return Results.Draw;
        return Results.Lose;
    }

    Elements[] BuildOptions()
    {
        Elements[] e = new Elements[3];
        e[0] = LosesTo(LeftElement);
        e[1] = LosesTo(MiddleElement);
        e[2] = LosesTo(RightElement);
        return e;
    }

    Elements GetChallenger()
    {
        Elements[] options = BuildOptions();
        return options[random.Next(3)];
    }


    void SetSprite(Elements e, GameObject thrown)
    {
        thrown.SetActive(true);
        switch(e)
        {
            case Elements.Rock: 
                thrown.GetComponent<SpriteRenderer>().sprite = RockSprite;
                break;
            case Elements.Scissors:
                thrown.GetComponent<SpriteRenderer>().sprite = ScissorsSprite;
                break;
            case Elements.Paper:
                thrown.GetComponent<SpriteRenderer>().sprite = PaperSprite;
                break;
        }
    }

    void SetSprites()
    {
        SetSprite(LeftElement, LeftThrown);
        SetSprite(MiddleElement, MiddleThrown);
        SetSprite(RightElement, RightThrown);
        SetSprite(ChallengerElement, ChallengerThrown);        
        StartTime = System.DateTime.Now;
        MaxTime = TimeForWins(Wins);
        if (WinsText != null)
            WinsText.GetComponent<TextMeshProUGUI>().SetText("" + Wins);
        LeftTimeIndicator.GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f);
        LeftTimeIndicator.gameObject.SetActive(true);
        RightTimeIndicator.GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f);
        RightTimeIndicator.gameObject.SetActive(true);
    }

    void Win()
    {
        if (GameOver) return;
        if (Storage.SoundOn)
            source.PlayOneShot(clipWin); 
        Wins++;
        StopClock = false;
        ChallengerElement = GetChallenger();
        ChallengerThrown.GetComponent<Transform>().position = HomePosition;
        AttackedGO = null;
        SetSprites();
    }

    void EndGame(Results result)
    {
        if (GameOver)
            return;
        GameOver = true;
        LeftTimeIndicator.SetActive(false);
        RightTimeIndicator.SetActive(false);
        Pointer.SetActive(false);
        if (result == Results.Delay)
        {
            Storage.Left = LeftElement;
            Storage.Middle = MiddleElement;
            Storage.Right = RightElement;
            Storage.Challenger = ChallengerElement;
            Storage.Sleeper = ChallengerElement;
            Storage.Winner = Elements.Error;
            Storage.Loser = Elements.Error;
            StartCoroutine(LoadYourAsyncScene("GameResults"));                
        }       
        else //Lose
        {
            if (Storage.SoundOn)
                source.PlayOneShot(clipLose);
            Dying = true;
            Storage.Left = LeftElement;
            Storage.Middle = MiddleElement;
            Storage.Right = RightElement;
            Storage.Challenger = ChallengerElement;
            Storage.Sleeper = Elements.Error;
            Storage.Winner = AttackedElement;
            Storage.Loser = ChallengerElement;        
            Storage.CurrentScore = Wins; 
        }
        

        Storage.CurrentScore = int.Parse(WinsText.GetComponent<TextMeshProUGUI>().GetParsedText());

    }

    void DrawGame()
    {
        AttackedGO = null;
        if (Storage.SoundOn)
            source.PlayOneShot(clipDraw);
    }

    void ChallengeLeft()
    {
        AttackedElement = LeftElement;
        AttackedGO = LeftThrown;
        switch(Challenge(ChallengerElement, LeftElement))
        {
            case Results.Win:
                LeftElement = ChallengerElement;
                Win();
                break;
            case Results.Lose:
                EndGame(Results.Lose);
                break;
            case Results.Draw:
                DrawGame();
                break;
        }
    }

    void ChallengeMiddle()
    {
        AttackedElement = MiddleElement;
        AttackedGO = MiddleThrown;
        switch (Challenge(ChallengerElement, MiddleElement))
        {
            case Results.Win:
                MiddleElement = ChallengerElement;
                Win();
                break;
            case Results.Lose:
                EndGame(Results.Lose);
                break;
            case Results.Draw:
                DrawGame();
                break;
        }
    }

    void ChallengeRight()
    {
        AttackedElement = RightElement;
        AttackedGO = RightThrown;
        switch (Challenge(ChallengerElement, RightElement))
        {
            case Results.Win:
                RightElement = ChallengerElement;
                Win();
                break;
            case Results.Lose:
                EndGame(Results.Lose);
                break;
            case Results.Draw:
                DrawGame();
                break;
        }
    }

    void Draw()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Move the object
        Vector3 targetPosition;
        float step = 1;
        if (Storage.Difficulty == Storage.DifficultyLevel.Easy)
            step = (float)EasySpeedForWins(Wins) * Time.deltaTime;
        else
            step = (float)HardSpeedForWins(Wins) * Time.deltaTime;


        if (Wins == 0)// && ChallengerThrown.GetComponent<Transform>().position == HomePosition)
        {
            float speed = 6f * Time.deltaTime;
            Vector3 tpos = new Vector3();
            Pointer.SetActive(true);
            switch (ChallengerElement)
            {
                case Elements.Rock:
                    tpos = RightThrown.GetComponent<Transform>().position;
                    break;
                case Elements.Paper:
                    tpos = LeftThrown.GetComponent<Transform>().position;
                    break;
                case Elements.Scissors:
                    tpos = MiddleThrown.GetComponent<Transform>().position;
                    break;
            }
            if (Pointer.GetComponent<Transform>().position == tpos)
                Pointer.GetComponent<Transform>().position = HomePosition;
            Pointer.GetComponent<Transform>().position = Vector3.MoveTowards(Pointer.GetComponent<Transform>().position, tpos, speed);
        }
        else
        {
            Pointer.SetActive(false);
        }

        if (AttackedGO != null)
        {
            if (Dying)
            {
                float speed = MovementSpeed*2.6f * Time.deltaTime;
                targetPosition = new Vector3(AttackedGO.GetComponent<Transform>().position.x, FallPoint.GetComponent<Transform>().position.y);
                ChallengerThrown.GetComponent<Transform>().position = Vector3.MoveTowards(ChallengerThrown.GetComponent<Transform>().position, targetPosition, speed);
                ChallengerThrown.GetComponent<Transform>().Rotate(new Vector3(0,0,10),Space.Self);

                if (ChallengerThrown.GetComponent<Transform>().position == targetPosition)
                {
                    StartCoroutine(LoadYourAsyncScene("GameResults"));
                    Dying = false;   
                }
            } else {
                targetPosition = AttackedGO.GetComponent<Transform>().position;
                ChallengerThrown.GetComponent<Transform>().position = Vector3.MoveTowards(ChallengerThrown.GetComponent<Transform>().position, targetPosition, step);
                if (ChallengerThrown.GetComponent<Transform>().position == targetPosition)
                {
                    if (AttackedGO == LeftThrown.gameObject)
                        ChallengeLeft();
                    else if (AttackedGO == MiddleThrown.gameObject)
                        ChallengeMiddle();
                    else if (AttackedGO == RightThrown.gameObject)
                        ChallengeRight();
                }
            }
        }
        else
        {
            ChallengerThrown.GetComponent<Transform>().position = Vector3.MoveTowards(ChallengerThrown.GetComponent<Transform>().position, HomePosition, step);
        }


        if (!StopClock) {
            //Do the scrollbars
            System.TimeSpan ts = System.DateTime.Now.Subtract(StartTime);
            double elapsed = ts.TotalMilliseconds;
            if (elapsed > MaxTime)
            {
                 LeftTimeIndicator.SetActive(false);
                 RightTimeIndicator.SetActive(false);
                 EndGame(Results.Delay);
            }
            else
            {
                LeftTimeIndicator.GetComponent<RectTransform>().localScale = new Vector3(1.0f - (float)(elapsed / MaxTime), 1.0f);
                RightTimeIndicator.GetComponent<RectTransform>().localScale = new Vector3(1.0f -+ (float)(elapsed / MaxTime), 1.0f);
            }
        }
    }

    public void SwipeDelta(Vector3 delta)
    {

    }

    public void SwipeStarted(Vector3 start)
    {
        Swiping = true;
    }

    public void SwipeEnded(Vector3 start, Vector3 stop)
    {
        if (GameOver || !Swiping) return;
        Swiping = false;
        float angle = Mathf.Atan2(stop.y-start.y, stop.x-start.x) * Mathf.Rad2Deg;
        if (angle < 170 && angle > 100)
            AttackedGO = LeftThrown.gameObject;
        else if (angle <= 100 && angle >= 80)
            AttackedGO = MiddleThrown.gameObject;
        else if (angle < 80 && angle > 10)
            AttackedGO = RightThrown.gameObject;
    }


    public void SwipeLeft()
    {

    }

    public void SwipeRight()
    {

    }

    public void SwipeUp()
    {

    }
   
    public void SwipeDown()
    {

    }

    public void SwipeStarted()
    {

    }


    public void Tap(Vector3 v)
    {

    }




    IEnumerator LoadYourAsyncScene(string name)
    {
        // The Application loads the Scene in the background as the current Scene runs.
        // This is particularly good for creating loading screens.
        // You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
        // a sceneBuildIndex of 1 as shown in Build Settings.

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(name);

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
