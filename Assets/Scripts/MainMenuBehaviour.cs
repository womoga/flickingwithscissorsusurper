﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuBehaviour : MonoBehaviour
{

    public Canvas canvas = null;
    public Vector2 referenceResolution;
    public GameObject[] objects;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void LoadEasyGamePlay()
    {
        Storage.Difficulty = Storage.DifficultyLevel.Easy;
        Storage.Continues = 1;
        Storage.CurrentScore = 0;
        StartCoroutine(LoadYourAsyncScene("GamePlay"));
    }

    void LoadHardGamePlay()
    {
        Storage.Difficulty = Storage.DifficultyLevel.Hard;
        Storage.Continues = 1;
        Storage.CurrentScore = 0;
        StartCoroutine(LoadYourAsyncScene("GamePlay"));
    }

    void LoadAbout()
    {
        StartCoroutine(LoadYourAsyncScene("About"));
    }

    IEnumerator LoadYourAsyncScene(string name)
    {
        // The Application loads the Scene in the background as the current Scene runs.
        // This is particularly good for creating loading screens.
        // You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
        // a sceneBuildIndex of 1 as shown in Build Settings.

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(name);

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }

    public void OnEasyButton()
    {
        LoadEasyGamePlay();
    }

    public void OnHardButton()
    {
        LoadHardGamePlay();
    }

    public void OnAboutButton()
    {
        LoadAbout();
    }

    public void OnExitButton()
    {
        Application.Quit();
    }
}
