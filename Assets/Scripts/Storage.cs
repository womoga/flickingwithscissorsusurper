﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Storage
{
    public enum DifficultyLevel { Easy, Hard };

    private static DifficultyLevel difficulty;

    private static int currentScore;

    private static int continues;

    private static GamePlayBehavior.Elements loser;

    private static GamePlayBehavior.Elements winner;

    private static GamePlayBehavior.Elements sleeper;

    private static GamePlayBehavior.Elements left;

    private static GamePlayBehavior.Elements middle;

    private static GamePlayBehavior.Elements right;

    private static GamePlayBehavior.Elements challenger;

    public static DifficultyLevel Difficulty {
        get {
            return difficulty;
        }
        set {
            difficulty = value;
        }
    }

    public static int EasyHighScore {
       get
        {
            return PlayerPrefs.GetInt("EasyHighScore", 0);
        }
       set
        {
            PlayerPrefs.SetInt("EasyHighScore", value);
        }
    }

    public static int HardHighScore
    {
        get
        {
            return PlayerPrefs.GetInt("HardHighScore", 0);
        }
        set
        {
            PlayerPrefs.SetInt("HardHighScore", value);
        }
    }

    public static int HighScore
    {
        get
        {
            if (difficulty == DifficultyLevel.Easy)
                return EasyHighScore;
            else
                return HardHighScore;
        }

        set
        {
            if (difficulty == DifficultyLevel.Easy)
                EasyHighScore = value;
            else
                HardHighScore = value;
        }
    }

    public static int CurrentScore
    {
        get
        {
            return currentScore;
        }

        set
        {
            currentScore = value;
        }
    }

    public static int Continues
    {
        get
        {
            return continues;
        }

        set
        {
            continues = value;
        }
    }

    public static GamePlayBehavior.Elements Sleeper
    {
        get
        {
            return sleeper;
        }

        set
        {
            sleeper = value;
        }
    }

    public static GamePlayBehavior.Elements Winner
    {
        get
        {
            return winner;
        }

        set
        {
            winner = value;
        }
    }

    public static GamePlayBehavior.Elements Loser
    {
        get
        {
            return loser;
        }

        set
        {
            loser = value;
        }
    }



    public static bool SoundOn
    {
        get
        {
            return PlayerPrefs.GetInt("SoundOn", 1) == 1;
        }
        set
        {
            if (value == false)
                PlayerPrefs.SetInt("SoundOn", 0);
            else
                PlayerPrefs.SetInt("SoundOn", 1);
        }
    }

    public static GamePlayBehavior.Elements Left
    {
        get
        {
            return left;
        }

        set
        {
            left = value;
        }
    }

    public static GamePlayBehavior.Elements Middle
    {
        get
        {
            return middle;
        }

        set
        {
            middle = value;
        }
    }

    public static GamePlayBehavior.Elements Right
    {
        get
        {
            return right;
        }

        set
        {
            right = value;
        }
    }

    public static GamePlayBehavior.Elements Challenger
    {
        get
        {
            return challenger;
        }

        set
        {
            challenger = value;
        }
    }
}
